# Envoie du serveur vers le client #

# le but de cette manipulation est de utiliser Scapy pour voir ce qui se passe quand on envoi une information entre un serveur vers un client
# pour connaitre par ou circule les informations.


>>> repse=sr( IP(dst='#Destinataire#', ttl=(1,25)) / TCP(), timeout=1 ) 
>>> repse.graph()                    # Représentation sous forme graphique      
